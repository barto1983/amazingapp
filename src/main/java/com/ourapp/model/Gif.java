package com.ourapp.model;

import com.ourapp.dao.GifDao;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class Gif implements GifDao {

    private List<String> gifList = new ArrayList<>();

    private String name;

    private int categoryId;

    public int getCategoryId(int cat) {
        return categoryId;
    }

    public String getName() {
        return name;
    }

    static List<Gif> gifs =new ArrayList<>();


    public Gif(String name, int categoryId) {
        this.name = name;
        this.categoryId = categoryId;
    }

    public Gif(){}

    static{
        gifs.add(new Gif("/gifs/android-explosion.gif",1));
        gifs.add(new Gif("/gifs/ben-and-mike.gif",1));
        gifs.add(new Gif("/gifs/book-dominos.gif",2));
        gifs.add(new Gif("/gifs/compiler-bot.gif",2));
        gifs.add(new Gif("/gifs/cowboy-coder.gif",3));
        gifs.add(new Gif("/gifs/infinite-andrew.gif",3));
    }

    @Override
    public List<String> findAll() {
        addGifs();
        return gifList;
    }

    public List<Gif> findAllGifs() {
        return gifs;
    }

    @Override
    public void addGifs() {
        gifList =  gifs.stream().map(gif -> gif.name).collect(Collectors.toList());
    }


}
