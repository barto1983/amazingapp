package com.ourapp.model;

import java.util.ArrayList;
import java.util.List;

public class Category {

    private int id;
    private String name;

    public Category() {}

    public Category(int id) {
        this.id = id;
        for (Category temp : getCategory()) {
            if(temp.getId()==id)
                this.name=temp.name;

        }
    }

    public Category(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Category> getCategory() {
        List<Category> category = new ArrayList<>();
        category.add(new Category(1, "Android"));
        category.add(new Category(2, "Funny"));
        category.add(new Category(3, "Programming"));
        return category;
    }

}
