package com.ourapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AmazingappApplication {

	public static void main(String[] args) {
		SpringApplication.run(AmazingappApplication.class, args);
	}
}
