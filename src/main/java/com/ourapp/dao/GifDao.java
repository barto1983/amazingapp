package com.ourapp.dao;

import java.util.List;

public interface GifDao {

    List<String> findAll();
    void addGifs();
}
