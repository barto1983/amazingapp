package com.ourapp.controller;

import com.ourapp.model.Category;
import com.ourapp.model.Gif;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.ArrayList;
import java.util.List;

@Controller
public class HomeController {

    @GetMapping("/")
    public String allGifs(ModelMap map) {
        Gif gifs = new Gif();
        map.put("gifs", gifs.findAll());
        return "home";
    }

    @GetMapping("/categories")
    public static String showCategories(ModelMap modelMap) {

        Category category = new Category();

        modelMap.put("categories", category.getCategory());
        return "categories";
    }

    /*@GetMapping("/category/{category.id}")
    public String getById(@PathVariable int categoryId, ModelMap map) {

        Category category = new Category();
        Gif gifs = new Gif();
        List<Gif> gifCat = new ArrayList<>();

        for (Category cat : category.getCategory()) {

            if (cat.getId() == gifs.getCategoryId(categoryId)){
                map.put("category", gifs.);
            }
            //map.put("category", gifs.);
        }
        return "category";
    }*/

    @GetMapping("/category/{category.id}")
    public String getById(@PathVariable("category.id") int categoryId, ModelMap map) {

        Category category = new Category();
        Gif gifs = new Gif();
        List<String> gifCat = new ArrayList<>();
        for (Gif gif : gifs.findAllGifs()) {

            if (gif.getCategoryId() == categoryId){
                gifCat.add(gif.getName());
                System.out.println(gif.getName());
            }
        }
        map.put("category", new Category(categoryId));
        map.put("category2", gifCat);

        return "category";
    }


}
